import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';
import { useFonts } from 'expo-font';
import AppLoading from 'expo-app-loading'; 
import CookStep from '../screen/CookStep';
import FoodCategory from '../screen/FoodCategory';
import FoodDetails from '../screen/FoodDetails';
import FoodRecipe from '../screen/FoodRecipe';
import FoodRecipeCategory from '../screen/FoodRecipeCategory';
import Login from '../screen/Login';
import About from '../screen/About';
import { AntDesign } from '@expo/vector-icons';



const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

export default function Router() {
     let [fontsLoaded] = useFonts({
        'Montserrat': require('../assets/fonts/Montserrat/Montserrat-Bold.ttf'),
        'Montserrat-light':require('../assets/fonts/Montserrat/Montserrat-Light.ttf')
      })
    
    if (!fontsLoaded) {
        return <AppLoading />;
      } else {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{
    headerShown: false
  }}> 
        <Stack.Screen name='login' component={Login}/>
        <Stack.Screen name='tab' component={TabHome}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
}


const TabHome = ()=>(
  <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let tabColor;

          if (route.name === 'foodes') {
            tabColor = focused
            ? '#FF5F28'
            : 'black';
           return <MaterialCommunityIcons name='bread-slice-outline' size={24} color={tabColor} />
          } else if (route.name === 'food-categorys') {
            tabColor = focused
            ? '#FF5F28'
            : 'black';
            return <MaterialIcons name="fastfood" size={24} color={tabColor} />
          }
          else if (route.name === 'about') {
            tabColor = focused
            ? '#FF5F28'
            : 'black';
            return <AntDesign name="user" size={24} color={tabColor} />
          }

          // You can return any component that you like here!
          
          
        },
        tabBarActiveTintColor: '#FF5F28',
        tabBarInactiveTintColor: 'gray',
        headerShown:false,
        
      })
    }
      
      >
        <Tab.Screen name="foodes" component={food} options={{ title:'food'}} />
        <Tab.Screen name="about" component={About} options={{headerShown:true, headerTitleAlign:'center',
        headerTitleStyle:{
            fontWeight: 'bold',
            fontSize:24,
            fontFamily:'Montserrat'
        },
        headerStyle: {
            backgroundColor: '#FF5F28',
        },}}/>
        <Tab.Screen name="food-categorys" component={foodCategory} />
      </Tab.Navigator>
)
const food = ()=>(
    
    <Stack.Navigator screenOptions={{
        headerTitleAlign:'center',
        headerTitleStyle:{
            fontWeight: 'bold',
            fontSize:24,
            fontFamily:'Montserrat'
        },
        headerStyle: {
            backgroundColor: '#FF5F28',
        },
        
        }}>
        <Stack.Screen name="food" component={FoodRecipe}  options={{title:'FOOD'}}/>
        <Stack.Screen name="foodDetails" component={FoodDetails}  options={{title:'FOOD DETAILS'}}/>
        <Stack.Screen name="CookStep" component={CookStep}  options={{title:'COOKING STEP'}}/>
        
    </Stack.Navigator>
)

const foodCategory= ()=>(
    
    <Stack.Navigator screenOptions={{
        headerTitleAlign:'center',
        headerTitleStyle:{
            fontWeight: 'bold',
            fontSize:24,
            fontFamily:'Montserrat'
        },
        headerStyle: {
            backgroundColor: '#FF5F28',
        },
      }}>
        <Stack.Screen name="food-category" component={FoodCategory} options={{title:'FOOD CATEGORY'}}/>
        <Stack.Screen name="foodRecipe-Category" component={FoodRecipeCategory} options={({ route }) => ({ title: route.params.header})}/>
        <Stack.Screen name="foodDetails" component={FoodDetails}  options={{title:'FOOD DETAILS'}}/>
        <Stack.Screen name="CookStep" component={CookStep}  options={{title:'COOKING STEP'}}/>
    </Stack.Navigator>
)