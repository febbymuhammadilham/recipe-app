import React, {useEffect, useContext} from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity , FlatList} from 'react-native'
import { useFonts } from 'expo-font';
import { FoodContext } from '../context/foodContext';


export default function FoodCategory({navigation}) {

    let [MontserratBold] = useFonts({
        'Montserrat-Bold':require('../assets/fonts/Montserrat/Montserrat-Bold.ttf'),
      })

     const [Food, detailFood, setKey, setFood, getFood, printToConsole,
        getDetails , printTo, getCategory, setCategory, category, getListCategory, listCategory ] = useContext(FoodContext)


    const listOpsiCategory = ({item})=>{
            return(
            <TouchableOpacity style={styles.content} onPress={()=>{
                getListCategory(item.key)
                navigation.navigate('foodRecipe-Category', {
                    header:item.category.toUpperCase()
                  })
            }}>
                <Text style={styles.text}>{item.category}</Text>
            </TouchableOpacity>
            )
    }

    return (
        <View style={styles.container}>
            <FlatList
                numColumns={2}
                    data={category}
                    renderItem={listOpsiCategory}
                    keyExtractor={item=>item.key}
             /> 
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignContent:'center'
    },

    content:{
        alignItems:'center',
        justifyContent:'center',
        width:'46%',
        height:49,
        margin:5,
        backgroundColor:'#C4C4C4',
        borderRadius:10,
        borderWidth:1

    },

    text:{
        fontSize:18,
        fontFamily:'Montserrat-Bold'
    }
})

