import React, {useContext, useEffect} from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity,ScrollView } from 'react-native'
import { useFonts } from 'expo-font';
import { Entypo } from '@expo/vector-icons';
import { FoodContext} from '../context/foodContext';
import { Ionicons } from '@expo/vector-icons';


export default function FoodDetails({navigation, route}) {
      
    let [fontsLoaded] = useFonts({
        'Montserrat-light':require('../assets/fonts/Montserrat/Montserrat-Light.ttf'),
      })
    
      let [MontserratBold] = useFonts({
        'Montserrat-Bold':require('../assets/fonts/Montserrat/Montserrat-Bold.ttf'),
      })

      const { key,title } = route.params;
      const [ Food, detailFood, setKey, setFood, getFood, printToConsole, getDetails , printTo ] = useContext(FoodContext)
      
   
    return (

        
        <ScrollView style={styles.container}>
            <View>
            <Image style={styles.imageFood}  source={{ uri:detailFood.thumb}}/>
            <Text style={styles.TitleFood}>{detailFood.title}</Text>
            <TouchableOpacity style={styles.Step}   onPress={() => navigation.navigate('CookStep', {
                    step:detailFood.step
                  })}>
            <Text style={styles.CookingStep}>Cooking Step </Text>
            
            <Entypo name="controller-play" size={24} color="black" />
            </TouchableOpacity>
            <Text style={styles.ingredients}>Ingredients :</Text>
            <Text style={styles.desc}>{detailFood.ingredient}</Text>
           

            <Text style={styles.ingredients}>Times:</Text>
            <Text style={styles.desc}>{detailFood.times}  <Ionicons name="timer" size={14} color="black" /></Text>

            <Text style={styles.ingredients}>Difficulty:</Text>
            <Text style={styles.desc}>{detailFood.dificulty}</Text>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,

    },
    ingredients:{
        fontSize:18,
        fontFamily:'Montserrat-Bold', 
        padding:12
    },
    desc:{
        justifyContent:'center',
        paddingHorizontal:12
    },
    imageFood:{
        width:'100%',
        height:250
    },
    TitleFood:{
        fontSize:18,
        fontFamily:'Montserrat-light',
        margin:12
    },
    Step:{
        flexDirection:'row',
        margin:12,
        alignContent:'space-between',
        backgroundColor:'#C4C4C4', 
        padding:12
    },

    CookingStep:{
        fontSize:18,
        fontFamily:'Montserrat-Bold', 
    }
})
