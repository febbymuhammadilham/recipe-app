import React, { useContext, useState }  from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity ,FlatList} from 'react-native'
import {styleRecipe }from '../style/FoodRecipeStyle'
import { useEffect } from 'react';
import { useFonts } from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
import { FoodContext} from '../context/foodContext';
import { color } from 'react-native-reanimated';

 





export default function FoodRecipe({navigation}) {
        
        let [fontsLoaded] = useFonts({
            'Montserrat-light':require('../assets/fonts/Montserrat/Montserrat-Light.ttf')
          })
          const [ Food, detailFood, setKey, setFood, getFood, printToConsole, getDetails , printTo ] = useContext(FoodContext)
         
     const listFood = ({item})=>{
         return(
            <TouchableOpacity   onPress={() =>{
                getDetails(item.key)
                navigation.navigate('foodDetails', {
                    key:item.key,
                    header:item.title
                  });
            }}>
            <View style={styles.content}>
                
                <Image style={styles.imageFood}
                 source={{ uri:item.thumb}}
                />
                <Text style={styles.TitleFood}>
                {item.title}</Text>
               
                <View style={{flex:1,flexDirection:'row', alignItems:"flex-end", justifyContent:'center', alignContent:'flex-end'}}>
                
                <Text style={{margin:3,alignContent:'flex-end'}}>{item.times}</Text>
                <Ionicons name="timer" size={24} color="black" />
                </View>
            </View>
          </TouchableOpacity>
        
         )
     }

    return (
        
 <View style={styles.container}>
       <FlatList
       numColumns={2}
        data={Food}
        renderItem={listFood}
        keyExtractor={item=>item.key}
       /> 
       
</View>
    
    // </foodProvider> 
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
    
    },
    content:{
        width:160,
        height:260, 
        borderWidth:1,
        borderRadius:10,
        margin:7,
        alignItems:'center'

    },
    imageFood:{
        width:160,
        height:160,
        borderRadius:10,

    },

    TitleFood:{
        fontSize:14,
        fontFamily:'Montserrat-light',
        margin:2
    },
    timer:{
        flex:1,
        flexDirection:'row',
       
    }
})
