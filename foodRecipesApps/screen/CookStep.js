import React from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity, FlatList } from 'react-native'
import { useFonts } from 'expo-font';

export default function CookStep({navigation,route}) {
    let [fontsLoaded] = useFonts({
        'Montserrat-light':require('../assets/fonts/Montserrat/Montserrat-Light.ttf'),
      })

      const {step}   = route.params;
      let tmp = [];
      for (let i = 0; i < step.length; i++) {
        tmp.push(i);
      }
      let indents = tmp.map(function (i) {
        return (
        
        <Text style={styles.textStep}><Text>#{i+1}.</Text> {step[i].substr(2)}</Text>
        );
      });
    return (
        
        <View>
            <View  style={styles.step}>
           
            {indents}
            
            </View>
        </View>
    )
}



const styles = StyleSheet.create({
    step:{
        padding:12,
        
    },
    number:{
       marginRight:2,
        fontSize:14
    },
    textStep:{
        // fontFamily:'Montserrat-Light',
        fontSize:14,
        padding:5,
        borderBottomWidth:1,
        borderBottomColor:'black',
        fontWeight:'bold'


    }
})