import React, {useContext,useState} from 'react'
import { View, Text, StyleSheet, Image, TextInput, TouchableOpacity } from 'react-native'
import { FoodContext} from '../context/foodContext';
import { useFonts } from 'expo-font';

export default function Login({navigation}) {

    let [fontsLoaded] = useFonts({
        'Montserrat-light':require('../assets/fonts/Montserrat/Montserrat-Light.ttf'),
      })

   const [Food, detailFood, setKey, setFood, getFood, printToConsole,
        getDetails , printTo, getCategory, setCategory, category, getListCategory, listCategory,
         setUsername, username] = useContext(FoodContext);
         
         const [name, setName] = useState("");
         const handleChangeName = (text) => {
            setName(text);
        };

    return (
        <View style={styles.container}>
            <Image source={require('../assets/icon-login.png')}/>
            <TextInput style={styles.input} placeholder={'masukan username'} onChangeText={handleChangeName}/>
            <TextInput style={styles.input} placeholder={'masukan password'}  secureTextEntry={true}/>
            <TouchableOpacity style={styles.button} onPress={
                ()=>{
                    setUsername(name)
                    navigation.navigate('tab')}}>
                <Text>login</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        fontFamily:'Montserrat-light',
        flex:1,
        backgroundColor:'#FF5F28',
        alignItems:'center',
        justifyContent:'center',
    },
    input:{
        width:'80%',
        height:45,
        paddingLeft:15,
        backgroundColor:'white',
        borderRadius:10,
        marginVertical:5
    },
    button:{
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#73BCF1',
        width:'80%',
        height:45,
        borderRadius:10,
        marginVertical:5
    }

})
