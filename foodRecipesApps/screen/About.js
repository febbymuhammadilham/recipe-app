import React, { useContext} from 'react'
import { View, Text, StyleSheet, Image, TextInput, TouchableOpacity } from 'react-native'
import { useFonts } from 'expo-font';
import { FoodContext} from '../context/foodContext';

export default function About() {

    const [Food, detailFood, setKey, setFood, getFood, printToConsole,
        getDetails , printTo, getCategory, setCategory, category, getListCategory, listCategory,
         setUsername, username] = useContext(FoodContext);

    let [fontsLoaded] = useFonts({
        'Montserrat-light':require('../assets/fonts/Montserrat/Montserrat-Light.ttf'),
      })

    return (
        <View style={styles.container}>
        <Image source={require('../assets/user.png')}/>
        <Text style={styles.username}>@{username}</Text>
        </View>
    )
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white',
        alignItems:'center',
        justifyContent:'flex-start',
        paddingTop:15
    },
    username:{
        fontSize:18,
        fontFamily:'Montserrat-light',
    }
})
