import React, { useContext } from "react";
import { FlatList, StyleSheet, Text, View } from "react-native";
import { foodContext } from "../context/foodRecipeContext";

const RecipeList = () => {
	const [recipe] = useContext(foodContext)

	return (
		<FlatList
			data={recipe}
            keyExtractor={item=>item.id}
			renderItem={({ item }) => (
				<View style={styles.listContainer}>
					<Text>{`Name : ${item.title}`}</Text>
					<Text>{`Length of Time : ${item.time}`}</Text>
				</View>
			)}
		/> 
	);
};

const styles = StyleSheet.create({
	listContainer: {
		borderWidth: 1,
		borderRadius: 8,
		borderColor: "#eaeaea",
		padding: 10,
		marginHorizontal: 10,
		marginVertical: 5,
	},
});

export default RecipeList;
