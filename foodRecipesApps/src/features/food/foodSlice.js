import { createSlice } from '@reduxjs/toolkit'
import axios from 'axios'

const initialState = {
  value: 0,
  data:0

}

export const foodSlice = createSlice({
  name: 'food',
  initialState,
  reducers: {
    increment: (state) => {
      // Redux Toolkit allows us to write "mutating" logic in reducers. It
      // doesn't actually mutate the state because it uses the Immer library,
      // which detects changes to a "draft state" and produces a brand new
      // immutable state based off those changes
      state.value += 1
      console.log(state.value)
    },
    decrement: (state) => {
      state.value -= 1
    },
    incrementByAmount: (state, action) => {
      state.value += action.payload
    },
    getData:(state, action)=>{
      const Data = async () => {
				try {
					const data = {
						title: action.payload.title,
						value: action.payload.value,
					};

					const res = await axios.get(
						"http://api-food-recipe.herokuapp.com/recipe",
					);
          console.log(res)
          return res
				} catch (error) {
					console.error(error);
				}
			};

			Data();
      

		
    }
  },
})

// Action creators are generated for each case reducer function
export const { increment, decrement, incrementByAmount ,getData} = foodSlice.actions

export default foodSlice.reducer