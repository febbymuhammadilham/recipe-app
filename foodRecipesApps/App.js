import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Router from './router';
import { store } from './src/app/store'
import { Provider } from 'react-redux'
import { FoodProvider } from './context/foodContext';
import Login from './screen/Login';
export default function App() {
  return (
  <FoodProvider>
  <Router/> 
  </FoodProvider>

  
    
   
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
