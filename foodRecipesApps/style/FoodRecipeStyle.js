import * as React from 'react';
import {StyleSheet } from 'react-native';

const styleRecipe = StyleSheet.create({
    content:{
        width:'50%',
        height:'30%',
        alignItems:'center'
    },
    imageContent:{
        width:160,
        height:320
    }
})

export {styleRecipe}