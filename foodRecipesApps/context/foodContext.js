import React, { useState,useEffect, createContext } from "react";
import axios from "axios";
import { NavigationContainer } from "@react-navigation/native";
export const FoodContext = createContext();

export const FoodProvider = ({ children, navigation}) => {

	const [Food, setFood] = useState(""); 
	const [key, setKey] = useState("");
	const [detailFood, setdetailFood] = useState("");
	const [category, setCategory] = useState("")
	const [listCategory, setListCategory] = useState("")
    const [username, setUsername] = useState("")
	
    const getFood = async () => {
		axios.get("https://masak-apa-tomorisakura.vercel.app/api/recipes/")
        .then(res=>{
            const data = res.data.results;
            // console.log("res :", data);
            setFood(data);

        })
        .catch(
            error=>{
                console.log("error :",error);
            }
        )
	};

	

	const getDetails = async (keyRev) => {
		axios.get(`https://masak-apa-tomorisakura.vercel.app/api/recipe/${keyRev}`)
        .then(res=>{
            const data = res.data.results;
            // console.log("res :", data);
            setdetailFood(data);

        })
        .catch(
            error=>{
                console.log("error :",error);
            }
        )
	};
	const printTo = () => console.log("ini fungsi");
	const printToConsole = () => console.log("Printed to console");

	const getCategory = async () => {
		axios.get("https://masak-apa-tomorisakura.vercel.app/api/categorys/recipes/")
        .then(res=>{
            const data = res.data.results;
            console.log("res :", data);
            setCategory(data);

        })
        .catch(
            error=>{
                console.log("error :",error);
            }
        )
	};
	const getListCategory = async (keyCategory) => {
		axios.get(`https://masak-apa-tomorisakura.vercel.app/api/categorys/recipes/${keyCategory}`)
        .then(res=>{
            const data = res.data.results;
            console.log("res :", data);
            setListCategory(data);

        })
        .catch(
            error=>{
                console.log("error :",error);
            }
        )
	};
	
	useEffect(()=>{
		getFood()
		getCategory();
		
	},[])
	
	return (
		<FoodContext.Provider value={[Food, detailFood, setKey, setFood, getFood, printToConsole,
		 getDetails , printTo, getCategory, setCategory, category, getListCategory, listCategory,
          setUsername, username]}>
			{children}
		</FoodContext.Provider>
	);
};
